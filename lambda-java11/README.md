# Helloworld typechecker or the extract transform script.

This project contains additional source codes and 21 json event data (with json files and POJO) to build a gson typechecker based on the schema designed. I used star schema by partitioning the event data to business facts, user browser properties dimension and the stral features captured by the mobile device.

The event data can be partitioned into 3 broad categories. They are business facts, user browser dimension and stralFeatures dimension.

## Read Instructions for data storage and processing

Read Instructions.pdf for the plan to solution architect with the event file(s), deploy resources, review data and schema from 21 selected different events, decide what files to store, store and extract and write the extract transform script. 

## Deploy the resources

Deploy the lambda-java11 to setup AWS Services with my local account.

## Review data and schema
Review data and schema from the 21 selected events schema using gson.
Parsing through gson is optional by default.
So we'll need to integrate a typechecker to enforce rules.

### There are three rules to the typechecker

* The schema designed in Java Json Object has excess json elements than the Json Event files. My typechecker will indicate 200 in the response but print "The given JSON object lacks some properties from the JSON string event".

* The Json Event files have newer json elements than the existing Java Json Object  for parsing - This is a new event and my typechecker will throw an exception to indicate 405 in the response.

* The schema designed in Java Json Object has the same schema as the Json Event files. This is ok and my typechecker will indicate 200 in the response.

### Annotation on the typechecker

* The annotation @AllKeysRequired (Properties.java, ThestralFeatures.java) enforces the three rules to the typechecker. 

I did not have time to develop another one for the business properties which is more required rather than optional.

```bash
sam build
use **./detectnewevent.sh** to test a new property to observe if the typechecker functions correctly.

- Detect new event from typechecking

Console Output
Invoking helloworld.App::handleRequest (java11)
Skip pulling image and use local one: public.ecr.aws/sam/emulation-java11:rapid-1.55.0-x86_64.

Mounting /home/leon-ubuntu/sampleSAM1/lambda-java11/.aws-sam/build/HelloWorldFunction as /var/task:ro,delegated inside runtime container
START RequestId: f933b30e-9cee-4220-8d73-7cffeff71404 Version: $LATEST
Picked up JAVA_TOOL_OPTIONS: -XX:+TieredCompilation -XX:TieredStopAtLevel=1
END RequestId: f933b30e-9cee-4220-8d73-7cffeff71404
REPORT RequestId: f933b30e-9cee-4220-8d73-7cffeff71404  Init Duration: 0.12 ms  Duration: 1175.69 ms    Billed Duration: 1176 ms        Memory Size: 512 MB     Max Memory Used: 512 MB
{"statusCode":405,"headers":{"X-Custom-Header":"application/json","Content-Type":"application/json"},"body":"com.google.gson.JsonParseException: The JSON string event has excess properties: [**THISISANEWPROPERTY**]"}

- To run typechecker

**./typecheck.sh** to perform local invoke to the serverless application to capture the test the above scenario
```
### Generate the class diagram from the POJO

Use the Java POJO to generate the class diagram or the architecture of the schema by Visual Paradigm.

These are the classes extracted to be used to design into MYSQL schema or decided later what to store.

Refer to Database Schema.jpg after reverse engineering the Java POJO with visual paradigm.

This is the final design that contains as_is (existing schema, json) and to_be (new schema, json) and will be used to store in mysql.

## Decide what files to store

* Refer to Instructions.pdf for the thinking process.

* Open json files in events/to_be  UnescapeGlimpseChangeMembershipStatus1.json, UnescapeGlimpseVitals1.json, UnescapeInteractHeader1.json, ... for samples.

The JSON after extract transform would be a json object with business fact, stralFeatures and user browser.

{
    "bf": {
      "event": "interactHeader",
      "eventId": 2359831603285284690,
      "BsPropertiesObject": {
        ...
      },
      "userId": "61a6a93291d43ddf2b16491a",
      "deviceId": "2eafc7be-40a7-43b2-bce5-b719cbef333a"
    },
    "stralFeatures": {
      "userId": "61a6a93291d43ddf2b16491a",
      "deviceId": "2eafc7be-40a7-43b2-bce5-b719cbef333a",
      "thestralFeatures": {
        ...
      }
    },
    "userBrowser": {
      "userId": "61a6a93291d43ddf2b16491a",
      "deviceId": "2eafc7be-40a7-43b2-bce5-b719cbef333a",
      "utcTimestampMs": 1638312248887,
      "serverUtcTimestampMs": 1638312248887,
      "properties": {
        ...
      }
    }
}
## Store and extract and write the extract transform script. 

* Refer to build.gradle. The SAM CLI installs dependencies defined in `HelloWorldFunction/build.gradle`, creates a deployment package, and saves it in the `.aws-sam/build` folder.

Dependencies include AWS Lambda, guava and gson.

implementation 'com.amazonaws:aws-lambda-java-core:1.2.1'
implementation 'com.amazonaws:aws-lambda-java-events:3.11.0'
implementation 'com.google.guava:guava:31.1-jre'
implementation 'com.google.code.gson:gson:2.9.1'
implementation 'io.swagger.core.v3:swagger-integration:2.2.2'

Language: Java

Version: 11

* The typechecker converts semistructured of events to structured for data storage in MySQL.
Due to the typechecker in extract transform script, storage errors could be minimized.

* Refer to resultEventData-1.txt for the typechecking test report.


### AWS 

* Refer to Instructions.pdf for sample of event processing on AWS Lambda after deployment.

* You may refer to the input json files to create the test event in AWS Lambda.

# lambda-java11

This project contains source code and supporting files for a serverless application that you can deploy with the SAM CLI. It includes the following files and folders.

- HelloWorldFunction/src/main - Code for the application's Lambda function.
- events - Invocation events that you can use to invoke the function.
- HelloWorldFunction/src/test - Unit tests for the application code. 
- template.yaml - A template that defines the application's AWS resources.

The application uses several AWS resources, including Lambda functions and an API Gateway API. These resources are defined in the `template.yaml` file in this project. You can update the template to add AWS resources through the same deployment process that updates your application code.

If you prefer to use an integrated development environment (IDE) to build and test your application, you can use the AWS Toolkit.  
The AWS Toolkit is an open source plug-in for popular IDEs that uses the SAM CLI to build and deploy serverless applications on AWS. The AWS Toolkit also adds a simplified step-through debugging experience for Lambda function code. See the following links to get started.

* [CLion](https://docs.aws.amazon.com/toolkit-for-jetbrains/latest/userguide/welcome.html)
* [GoLand](https://docs.aws.amazon.com/toolkit-for-jetbrains/latest/userguide/welcome.html)
* [IntelliJ](https://docs.aws.amazon.com/toolkit-for-jetbrains/latest/userguide/welcome.html)
* [WebStorm](https://docs.aws.amazon.com/toolkit-for-jetbrains/latest/userguide/welcome.html)
* [Rider](https://docs.aws.amazon.com/toolkit-for-jetbrains/latest/userguide/welcome.html)
* [PhpStorm](https://docs.aws.amazon.com/toolkit-for-jetbrains/latest/userguide/welcome.html)
* [PyCharm](https://docs.aws.amazon.com/toolkit-for-jetbrains/latest/userguide/welcome.html)
* [RubyMine](https://docs.aws.amazon.com/toolkit-for-jetbrains/latest/userguide/welcome.html)
* [DataGrip](https://docs.aws.amazon.com/toolkit-for-jetbrains/latest/userguide/welcome.html)
* [VS Code](https://docs.aws.amazon.com/toolkit-for-vscode/latest/userguide/welcome.html)
* [Visual Studio](https://docs.aws.amazon.com/toolkit-for-visual-studio/latest/user-guide/welcome.html)

## Deploy the sample application

The Serverless Application Model Command Line Interface (SAM CLI) is an extension of the AWS CLI that adds functionality for building and testing Lambda applications. It uses Docker to run your functions in an Amazon Linux environment that matches Lambda. It can also emulate your application's build environment and API.

To use the SAM CLI, you need the following tools.

* SAM CLI - [Install the SAM CLI](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html)
* Java11 - [Install the Java 11](https://docs.aws.amazon.com/corretto/latest/corretto-11-ug/downloads-list.html)
* Docker - [Install Docker community edition](https://hub.docker.com/search/?type=edition&offering=community)

To build and deploy your application for the first time, run the following in your shell:

```bash
sam build
sam deploy --guided
```

The first command will build the source of your application. The second command will package and deploy your application to AWS, with a series of prompts:

* **Stack Name**: The name of the stack to deploy to CloudFormation. This should be unique to your account and region, and a good starting point would be something matching your project name.
* **AWS Region**: The AWS region you want to deploy your app to.
* **Confirm changes before deploy**: If set to yes, any change sets will be shown to you before execution for manual review. If set to no, the AWS SAM CLI will automatically deploy application changes.
* **Allow SAM CLI IAM role creation**: Many AWS SAM templates, including this example, create AWS IAM roles required for the AWS Lambda function(s) included to access AWS services. By default, these are scoped down to minimum required permissions. To deploy an AWS CloudFormation stack which creates or modifies IAM roles, the `CAPABILITY_IAM` value for `capabilities` must be provided. If permission isn't provided through this prompt, to deploy this example you must explicitly pass `--capabilities CAPABILITY_IAM` to the `sam deploy` command.
* **Save arguments to samconfig.toml**: If set to yes, your choices will be saved to a configuration file inside the project, so that in the future you can just re-run `sam deploy` without parameters to deploy changes to your application.

You can find your API Gateway Endpoint URL in the output values displayed after deployment.

## Use the SAM CLI to build and test locally

Build your application with the `sam build` command.

```bash
lambda-java11$ sam build
```

The SAM CLI installs dependencies defined in `HelloWorldFunction/build.gradle`, creates a deployment package, and saves it in the `.aws-sam/build` folder.

Test a single function by invoking it directly with a test event. An event is a JSON document that represents the input that the function receives from the event source. Test events are included in the `events` folder in this project.

Run functions locally and invoke them with the `sam local invoke` command.

```bash
lambda-java11$ sam local invoke HelloWorldFunction --event events/event.json
```

The SAM CLI can also emulate your application's API. Use the `sam local start-api` to run the API locally on port 3000.

```bash
lambda-java11$ sam local start-api
lambda-java11$ curl http://localhost:3000/
```

The SAM CLI reads the application template to determine the API's routes and the functions that they invoke. The `Events` property on each function's definition includes the route and method for each path.

```yaml
      Events:
        HelloWorld:
          Type: Api
          Properties:
            Path: /hello
            Method: get
```

## Add a resource to your application
The application template uses AWS Serverless Application Model (AWS SAM) to define application resources. AWS SAM is an extension of AWS CloudFormation with a simpler syntax for configuring common serverless application resources such as functions, triggers, and APIs. For resources not included in [the SAM specification](https://github.com/awslabs/serverless-application-model/blob/master/versions/2016-10-31.md), you can use standard [AWS CloudFormation](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-template-resource-type-ref.html) resource types.

## Fetch, tail, and filter Lambda function logs

To simplify troubleshooting, SAM CLI has a command called `sam logs`. `sam logs` lets you fetch logs generated by your deployed Lambda function from the command line. In addition to printing the logs on the terminal, this command has several nifty features to help you quickly find the bug.

`NOTE`: This command works for all AWS Lambda functions; not just the ones you deploy using SAM.

```bash
lambda-java11$ sam logs -n HelloWorldFunction --stack-name lambda-java11 --tail
```

You can find more information and examples about filtering Lambda function logs in the [SAM CLI Documentation](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-logging.html).

## Unit tests

Tests are defined in the `HelloWorldFunction/src/test` folder in this project.

```bash
lambda-java11$ cd HelloWorldFunction
HelloWorldFunction$ gradle test
```

## Cleanup

To delete the sample application that you created, use the AWS CLI. Assuming you used your project name for the stack name, you can run the following:

```bash
aws cloudformation delete-stack --stack-name lambda-java11
```

## Resources

See the [AWS SAM developer guide](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/what-is-sam.html) for an introduction to SAM specification, the SAM CLI, and serverless application concepts.

Next, you can use AWS Serverless Application Repository to deploy ready to use Apps that go beyond hello world samples and learn how authors developed their applications: [AWS Serverless Application Repository main page](https://aws.amazon.com/serverless/serverlessrepo/)
