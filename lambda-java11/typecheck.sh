#! /bin/bash
set -e
echo 'changeMembershipStatus'
sam local invoke HelloWorldFunction --event events/as_is/changeMembershipStatus1.json
echo 'clickViewCart'
sam local invoke HelloWorldFunction --event events/as_is/clickViewCart1.json
echo 'createAccount'
sam local invoke HelloWorldFunction --event events/as_is/createAccount1.json
echo 'device id used'
sam local invoke HelloWorldFunction --event events/as_is/deviceIdUsed1.json
echo 'flow PDP'
sam local invoke HelloWorldFunction --event events/as_is/flowPDP1.json
echo 'flow PLE'
sam local invoke HelloWorldFunction --event events/as_is/flowPLE1.json
echo 'full text search'
sam local invoke HelloWorldFunction --event events/as_is/fullTextSearch1.json
echo 'glimpseModal'
sam local invoke HelloWorldFunction --event events/as_is/glimpseModal1.json
echo 'glimpseModule'
sam local invoke HelloWorldFunction --event events/as_is/glimpseModule1.json
echo 'glimpsePle\n'
sam local invoke HelloWorldFunction --event events/as_is/glimpsePle1.json
echo 'interactHeader\n'
sam local invoke HelloWorldFunction --event events/as_is/interactHeader1.json
echo 'login\n'
sam local invoke HelloWorldFunction --event events/as_is/login1.json
echo 'login client\n'
sam local invoke HelloWorldFunction --event events/as_is/loginClient1.json
echo 'page view\n'
sam local invoke HelloWorldFunction --event events/as_is/pageView1.json
echo 'shipping address change\n'
sam local invoke HelloWorldFunction --event events/as_is/shippingAddressChange1.json
echo 'tag commercial user\n'
sam local invoke HelloWorldFunction --event events/as_is/tagCommercialUser1.json
echo 'tap add to cart\n'
sam local invoke HelloWorldFunction --event events/as_is/tapAddToCart1.json
echo 'tap category\n'
sam local invoke HelloWorldFunction --event events/as_is/tapCategory1.json
echo 'view cart\n'
sam local invoke HelloWorldFunction --event events/as_is/viewCart1.json
echo 'view mini cart\n'
sam local invoke HelloWorldFunction --event events/as_is/viewMiniCart1.json
echo 'web vitals\n'
sam local invoke HelloWorldFunction --event events/as_is/webVitals1.json






