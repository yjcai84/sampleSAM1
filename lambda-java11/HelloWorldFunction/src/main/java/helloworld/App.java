/**
 * Author: Cai Yuejun Leon
 * 
 * This App is a AWS Lambda Application to extract trasform 21 known json events from as_is to to_be.
 */
package helloworld;
import java.lang.System;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import helloworld.as_is.Properties;
import helloworld.to_be.BusinessFact;
import helloworld.to_be.DimStralFeatures;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent; 

public class App implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {
    private String EMPTYSTR = "";
    /** 
     * Response event from the API Gateway Proxy for AWS Lambda
     * 
     * @param input APIGatewayProxyRequestEvent to pull the event out from the request's body.
     * @param context Context
     * @return APIGatewayProxyResponseEvent to put the extract transformed event back to the response.
     */
    public APIGatewayProxyResponseEvent handleRequest(final APIGatewayProxyRequestEvent input, final Context context) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("X-Custom-Header", "application/json");
        APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent().withHeaders(headers);
        // LambdaLogger logger = context.getLogger();
        try {
            final String pageContents = this.getPageContents("https://checkip.amazonaws.com");

            String output = EMPTYSTR;
            output = String.format("{ \"message\": \"hello world\", \"location\": \"%s\", \"root\": \"%s\"}", pageContents, "");
            
            Map<String,String> accessControlForAPIGateway = new HashMap<String,String>(3);
            // otherwise the test will fail.
            if (input != null) { 
                // typecheck if there are json object that the json file does not have.
                GsonBuilder builder = new GsonBuilder();
                builder.setPrettyPrinting(); 
                builder.serializeNulls();
                builder.registerTypeAdapterFactory(AllKeysRequiredTypeAdapterFactory.get()).create();
                Gson gson = builder.create(); 
                String restMethod = input.getHttpMethod();
                // Root must be able to merge all the events and set the hhvm file as
                helloworld.as_is.Root rawObject = gson.fromJson(input.getBody(), helloworld.as_is.Root.class);
                helloworld.to_be.Root toBeObject = new helloworld.to_be.Root();
                if (restMethod.equals("POST")) {
                    toBeObject = extractTransform(rawObject);
                }

                accessControlForAPIGateway.put("Access-Control-Allow-Headers", "Content-Type");
                accessControlForAPIGateway.put("Access-Control-Allow-Origin", "");
                accessControlForAPIGateway.put("Access-Control-Allow-Methods", "OPTIONS,POST,GET");
                
                return response.withStatusCode(200).withBody(gson.toJson(toBeObject)).withHeaders(accessControlForAPIGateway);
            }
            accessControlForAPIGateway.put("Access-Control-Allow-Headers", "Content-Type");
            accessControlForAPIGateway.put("Access-Control-Allow-Origin", "");
            accessControlForAPIGateway.put("Access-Control-Allow-Methods", "OPTIONS,POST,GET");

            return response.withStatusCode(200).withBody(output);
        } catch (IOException ex) {
            return response
                .withBody("{}")
                .withStatusCode(500);
        } catch (Exception ex) {
            response.setStatusCode(405);
            response.setBody(ex.toString());
            return response;
        }
    }

    


    private static boolean compareJson(JsonElement json1, JsonElement json2) {
        boolean isEqual = true;
        // Check whether both jsonElement are not null
        if(json1 !=null && json2 !=null) {
            
            // Check whether both jsonElement are objects
            if (json1.isJsonObject() && json2.isJsonObject()) {
                Set<Entry<String, JsonElement>> ens1 = ((JsonObject) json1).entrySet();
                Set<Entry<String, JsonElement>> ens2 = ((JsonObject) json2).entrySet();
                JsonObject json2obj = (JsonObject) json2;
                if (ens1 != null && ens2 != null && (ens2.size() == ens1.size())) {
                    // Iterate JSON Elements with Key values
                    for (Entry<String, JsonElement> en : ens1) {
                        isEqual = isEqual && compareJson(en.getValue() , json2obj.get(en.getKey()));
                    }
                } else {
                    return false;
                }
            } 
            
            // Check whether both jsonElement are arrays
            else if (json1.isJsonArray() && json2.isJsonArray()) {
                JsonArray jarr1 = json1.getAsJsonArray();
                JsonArray jarr2 = json2.getAsJsonArray();
                if(jarr1.size() != jarr2.size()) {
                    return false;
                } else {
                    int i = 0;
                    // Iterate JSON Array to JSON Elements
                    for (JsonElement je : jarr1) {
                        isEqual = isEqual && compareJson(je , jarr2.get(i));
                        i++;
                    }   
                }
            }
            
            // Check whether both jsonElement are null
            else if (json1.isJsonNull() && json2.isJsonNull()) {
                return true;
            } 
            
            // Check whether both jsonElement are primitives
            else if (json1.isJsonPrimitive() && json2.isJsonPrimitive()) {
                if(json1.equals(json2)) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else if(json1 == null && json2 == null) {
            return true;
        } else {
            return false;
        }
        return isEqual;
    }






    /** 
     * Get Page Content from specific Url Address.
     * 
     * @param address Url Address
     * @return String Page Content.
     * @throws IOException
     */
    private String getPageContents(String address) throws IOException{
        URL url = new URL(address);
        try(BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()))) {
            return br.lines().collect(Collectors.joining(System.lineSeparator()));
        }
    }

    
    /** 
     * Extract Transform the as_is to to_be.
     * 
     * @param rawObject as_is Json Object.
     * @return Root to_be Json Object.
     */
    private helloworld.to_be.Root extractTransform(helloworld.as_is.Root rawObject) {
        
        helloworld.to_be.Root root = new helloworld.to_be.Root();

        // I use gson to map to the new schema after splitting fact and dimensions.

        BusinessFact bf = new BusinessFact();

        Properties properties = rawObject.getProperties();
        // setting business facts
        bf.setDeviceId(properties.getDeviceId());
        bf.setUserId(properties.getUserId());
        bf.setEventId(rawObject.getEventId());
        bf.setEvent(rawObject.getEvent());
        // shifting the business properties to fact table.
        bf.setBsProperties(properties.getBsProperties());

        DimStralFeatures dimStralFeatures = new DimStralFeatures();
        dimStralFeatures.setDeviceId(properties.getDeviceId());
        dimStralFeatures.setUserId(properties.getUserId());
        dimStralFeatures.setThestralFeatures(rawObject.getProperties().getThestralFeatures());
        dimStralFeatures.setDeviceId(properties.getDeviceId());
        dimStralFeatures.setUserId(properties.getUserId());

        // for user browser tracking - set individually
        helloworld.to_be.DimUserBrowser dimUserBrowser = new helloworld.to_be.DimUserBrowser();
        dimUserBrowser.setDeviceId(properties.getDeviceId());
        dimUserBrowser.setUserId(properties.getUserId());
        dimUserBrowser.setServerUtcTimestampMs(rawObject.getServerUtcTimestampMs());
        dimUserBrowser.setUtcTimestampMs(rawObject.getServerUtcTimestampMs());
        Properties dimUserBrowserProperties = rawObject.getProperties();
        dimUserBrowserProperties.setBsProperties(null);
        dimUserBrowserProperties.setThestralFeatures(null);
        dimUserBrowser.setProperties(dimUserBrowserProperties);

        root.setBF(bf);
        root.setUserBrowser(dimUserBrowser);
        root.setStralFeatures(dimStralFeatures);
        
        return root;
    }
}
