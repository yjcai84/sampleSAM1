package helloworld.to_be;

public class Root {
    
    private BusinessFact bf;
    
    private DimStralFeatures stralFeatures;
    
    private DimUserBrowser userBrowser;

    public BusinessFact getBF() {
        return this.bf;
    }

    public void setBF(BusinessFact bf) {
        this.bf = bf;
    }

    public DimStralFeatures getStralFeatures() {
        return stralFeatures;
    }

    public void setStralFeatures(DimStralFeatures stralFeatures) {
        this.stralFeatures = stralFeatures;
    }

    public DimUserBrowser getUserBrowser() {
        return userBrowser;
    } 
    
    public void setUserBrowser(DimUserBrowser userBrowser) {
        this.userBrowser = userBrowser;
    }
}
