package helloworld.to_be;

import helloworld.as_is.ThestralFeatures;

public class DimStralFeatures {
    private String userId;
    private String deviceId;
    private ThestralFeatures thestralFeatures;
    
    public String getUserId() {
      return userId;
    }

    public String getDeviceId() {
      return deviceId;
    }

    public ThestralFeatures getThestralFeatures() {
      return thestralFeatures;
    }

    public void setUserId(String userId) {
      this.userId = userId;
    }

    public void setDeviceId(String deviceId) {
      this.deviceId = deviceId;
    }

    public void setThestralFeatures(ThestralFeatures thestralFeatures) {
      this.thestralFeatures = thestralFeatures;
    }

  }
