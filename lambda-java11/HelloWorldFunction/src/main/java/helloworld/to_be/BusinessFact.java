package helloworld.to_be;
public class BusinessFact {
    private String event;
    private Long eventId;
    helloworld.as_is.BsProperties BsPropertiesObject;
    private String userId;
    private String deviceId;
    // Getter Methods 
    public String getEvent() {
      return event;
    }
  
    public float getEventId() {
      return eventId;
    }
  
    public helloworld.as_is.BsProperties getBsProperties() {
      return BsPropertiesObject;
    }
  
    public String getUserId() {
      return userId;
    }
  
    public String getDeviceId() {
      return deviceId;
    }
    // Setter Methods 
    public void setEvent( String event ) {
      this.event = event;
    }
  
    public void setEventId( Long eventId ) {
      this.eventId = eventId;
    }
  
    public void setBsProperties(helloworld.as_is.BsProperties bsPropertiesObject ) {
      this.BsPropertiesObject = bsPropertiesObject;
    }
  
    public void setUserId( String userId ) {
      this.userId = userId;
    }
  
    public void setDeviceId( String deviceId ) {
      this.deviceId = deviceId;
    }
}
