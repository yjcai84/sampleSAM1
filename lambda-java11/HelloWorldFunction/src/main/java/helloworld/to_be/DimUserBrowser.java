package helloworld.to_be;

import helloworld.as_is.Properties;

public class DimUserBrowser {
    private String userId;
    private String deviceId;
    private Long utcTimestampMs;
    private Long serverUtcTimestampMs;
    private helloworld.as_is.Properties properties;
    
    // Getter Methods 
    public String getUserId() {
      return userId;
    }

    public void setUserId(String userId) {
      this.userId = userId;
    }
  
    public String getDeviceId() {
      return deviceId;
    }

    public void setDeviceId(String deviceId) {
      this.deviceId = deviceId;
    }

    public Long getUtcTimestampMs() {
      return utcTimestampMs;
    }

    public void setUtcTimestampMs(Long utcTimestampMs) {
      this.utcTimestampMs = utcTimestampMs;
    }
  
    public Long getServerUtcTimestampMs() {
      return serverUtcTimestampMs;
    }

    public void setServerUtcTimestampMs(Long serverUtcTimestampMs) {
      this.serverUtcTimestampMs = serverUtcTimestampMs;
    }

    public helloworld.as_is.Properties getProperties() {
      return properties;
    }
  
    public void setProperties (Properties properties) {
      this.properties = properties;
    }
  
}
