package helloworld.as_is;
public class Report {
    private boolean priceOptimized;
    private boolean holdout;
    // Getter Methods 
    public boolean getPriceOptimized() {
      return priceOptimized;
    }
  
    public boolean getHoldout() {
      return holdout;
    }
  
   // Setter Methods 
  
    public void setPriceOptimized( boolean priceOptimized ) {
      this.priceOptimized = priceOptimized;
    }
  
    public void setHoldout( boolean holdout ) {
      this.holdout = holdout;
    }
  }