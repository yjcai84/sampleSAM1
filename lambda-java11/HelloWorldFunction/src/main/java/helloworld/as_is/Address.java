package helloworld.as_is;
public class Address {
    TelephoneVerification TelephoneVerificationObject;
    private boolean ignoreAddressVerification;
    private float maximumShipmentWeight;
    private boolean isCommercialAddress;
    private float shippingLabelProfile;
    private float accountScope;
    private String _id;
    private String firstName;
    private String lastName;
    private String addressLine1;
    private String addressLine2;
    private String city;
    private String state;
    private String postalCode;
    private String country;
    private String latitude = null;
    private String longitude = null;
    private String user;
    private String createdAt;
    private String updatedAt;
    private float __v;
    
    
    // Getter Methods 
    
    public TelephoneVerification getTelephoneVerification() {
        return TelephoneVerificationObject;
    }
    
    public boolean getIgnoreAddressVerification() {
        return ignoreAddressVerification;
    }
    
    public float getMaximumShipmentWeight() {
        return maximumShipmentWeight;
    }
    
    public boolean getIsCommercialAddress() {
        return isCommercialAddress;
    }
    
    public float getShippingLabelProfile() {
        return shippingLabelProfile;
    }
    
    public float getAccountScope() {
        return accountScope;
    }
    
    public String get_id() {
        return _id;
    }
    
    public String getFirstName() {
        return firstName;
    }
    
    public String getLastName() {
        return lastName;
    }
    
    public String getAddressLine1() {
        return addressLine1;
    }
    
    public String getAddressLine2() {
        return addressLine2;
    }
    
    public String getCity() {
        return city;
    }
    
    public String getState() {
        return state;
    }
    
    public String getPostalCode() {
        return postalCode;
    }
    
    public String getCountry() {
        return country;
    }
    
    public String getLatitude() {
        return latitude;
    }
    
    public String getLongitude() {
        return longitude;
    }
    
    public String getUser() {
        return user;
    }
    
    public String getCreatedAt() {
        return createdAt;
    }
    
    public String getUpdatedAt() {
        return updatedAt;
    }
    
    public float get__v() {
        return __v;
    }
    
    // Setter Methods 
    
    public void setTelephoneVerification( TelephoneVerification telephoneVerificationObject ) {
        this.TelephoneVerificationObject = telephoneVerificationObject;
    }
    
    public void setIgnoreAddressVerification( boolean ignoreAddressVerification ) {
        this.ignoreAddressVerification = ignoreAddressVerification;
    }
    
    public void setMaximumShipmentWeight( float maximumShipmentWeight ) {
        this.maximumShipmentWeight = maximumShipmentWeight;
    }
    
    public void setIsCommercialAddress( boolean isCommercialAddress ) {
        this.isCommercialAddress = isCommercialAddress;
    }
    
    public void setShippingLabelProfile( float shippingLabelProfile ) {
        this.shippingLabelProfile = shippingLabelProfile;
    }
    
    public void setAccountScope( float accountScope ) {
        this.accountScope = accountScope;
    }
    
    public void set_id( String _id ) {
        this._id = _id;
    }
    
    public void setFirstName( String firstName ) {
        this.firstName = firstName;
    }
    
    public void setLastName( String lastName ) {
        this.lastName = lastName;
    }
    
    public void setAddressLine1( String addressLine1 ) {
        this.addressLine1 = addressLine1;
    }
    
    public void setAddressLine2( String addressLine2 ) {
        this.addressLine2 = addressLine2;
    }
    
    public void setCity( String city ) {
        this.city = city;
    }
    
    public void setState( String state ) {
        this.state = state;
    }
    
    public void setPostalCode( String postalCode ) {
        this.postalCode = postalCode;
    }
    
    public void setCountry( String country ) {
        this.country = country;
    }
    
    public void setLatitude( String latitude ) {
        this.latitude = latitude;
    }
    
    public void setLongitude( String longitude ) {
        this.longitude = longitude;
    }
    
    public void setUser( String user ) {
        this.user = user;
    }
    
    public void setCreatedAt( String createdAt ) {
        this.createdAt = createdAt;
    }
    
    public void setUpdatedAt( String updatedAt ) {
        this.updatedAt = updatedAt;
    }
    
    public void set__v( float __v ) {
        this.__v = __v;
    }
}
