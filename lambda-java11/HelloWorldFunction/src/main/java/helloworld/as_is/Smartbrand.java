package helloworld.as_is;
public class Smartbrand {
  
    private float variantGid;
    private String adGid = null;
    private String adGroupGid = null;
    private String campaignGid = null;
    private String adId;
    private String adGroupId;
    private String campaignId;
    private String keywordBidId;
    private float adCostMicro;
    private float adCpcMicro;
    private String placement;
  
    // Getter Methods 
    public float getVariantGid() {
      return variantGid;
    }
  
    public String getAdGid() {
      return adGid;
    }
  
    public String getAdGroupGid() {
      return adGroupGid;
    }
  
    public String getCampaignGid() {
      return campaignGid;
    }
  
    public String getAdId() {
      return adId;
    }
  
    public String getAdGroupId() {
      return adGroupId;
    }
  
    public String getCampaignId() {
      return campaignId;
    }
  
    public String getKeywordBidId() {
      return keywordBidId;
    }
  
    public float getAdCostMicro() {
      return adCostMicro;
    }
  
    public float getAdCpcMicro() {
      return adCpcMicro;
    }
  
    public String getPlacement() {
      return placement;
    }
  
   // Setter Methods 
  
    public void setVariantGid( float variantGid ) {
      this.variantGid = variantGid;
    }
  
    public void setAdGid( String adGid ) {
      this.adGid = adGid;
    }
  
    public void setAdGroupGid( String adGroupGid ) {
      this.adGroupGid = adGroupGid;
    }
  
    public void setCampaignGid( String campaignGid ) {
      this.campaignGid = campaignGid;
    }
  
    public void setAdId( String adId ) {
      this.adId = adId;
    }
  
    public void setAdGroupId( String adGroupId ) {
      this.adGroupId = adGroupId;
    }
  
    public void setCampaignId( String campaignId ) {
      this.campaignId = campaignId;
    }
  
    public void setKeywordBidId( String keywordBidId ) {
      this.keywordBidId = keywordBidId;
    }
  
    public void setAdCostMicro( float adCostMicro ) {
      this.adCostMicro = adCostMicro;
    }
  
    public void setAdCpcMicro( float adCpcMicro ) {
      this.adCpcMicro = adCpcMicro;
    }
  
    public void setPlacement( String placement ) {
      this.placement = placement;
    }
}
