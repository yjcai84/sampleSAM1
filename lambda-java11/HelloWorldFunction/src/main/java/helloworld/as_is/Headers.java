package helloworld.as_is;
public class Headers {
    private String accept;
    private String acceptEncoding;
    private String acceptLanguage;
    private String cacheControl;
    private String cloudFrontForwardedProto;
    private String cloudFrontIsDesktopViewer;
    private String cloudFrontIsMobileViewer;
    private String cloudFrontIsSmartTVViewer;
    private String cloudFrontIsTabletViewer;
    private String cloudFrontViewerCountry;
    private String host;
    private String upgradeInsecureRequests;
    private String userAgent;
    private String via;
    private String xAmzCfId;
    private String xForwardedFor;
    private String xForwardedPort;
    private String xForwardedProto;
    public String getAccept() {
        return accept;
    }
    public void setAccept(String accept) {
        this.accept = accept;
    }
    public String getAcceptEncoding() {
        return acceptEncoding;
    }
    public void setAcceptEncoding(String acceptEncoding) {
        this.acceptEncoding = acceptEncoding;
    }
    public String getAcceptLanguage() {
        return acceptLanguage;
    }
    public void setAcceptLanguage(String acceptLanguage) {
        this.acceptLanguage = acceptLanguage;
    }
    public String getCacheControl() {
        return cacheControl;
    }
    public void setCacheControl(String cacheControl) {
        this.cacheControl = cacheControl;
    }
    public String getCloudFrontForwardedProto() {
        return cloudFrontForwardedProto;
    }
    public void setCloudFrontForwardedProto(String cloudFrontForwardedProto) {
        this.cloudFrontForwardedProto = cloudFrontForwardedProto;
    }
    public String getCloudFrontIsDesktopViewer() {
        return cloudFrontIsDesktopViewer;
    }
    public void setCloudFrontIsDesktopViewer(String cloudFrontIsDesktopViewer) {
        this.cloudFrontIsDesktopViewer = cloudFrontIsDesktopViewer;
    }
    public String getCloudFrontIsMobileViewer() {
        return cloudFrontIsMobileViewer;
    }
    public void setCloudFrontIsMobileViewer(String cloudFrontIsMobileViewer) {
        this.cloudFrontIsMobileViewer = cloudFrontIsMobileViewer;
    }
    public String getCloudFrontIsSmartTVViewer() {
        return cloudFrontIsSmartTVViewer;
    }
    public void setCloudFrontIsSmartTVViewer(String cloudFrontIsSmartTVViewer) {
        this.cloudFrontIsSmartTVViewer = cloudFrontIsSmartTVViewer;
    }
    public String getCloudFrontIsTabletViewer() {
        return cloudFrontIsTabletViewer;
    }
    public void setCloudFrontIsTabletViewer(String cloudFrontIsTabletViewer) {
        this.cloudFrontIsTabletViewer = cloudFrontIsTabletViewer;
    }
    public String getCloudFrontViewerCountry() {
        return cloudFrontViewerCountry;
    }
    public void setCloudFrontViewerCountry(String cloudFrontViewerCountry) {
        this.cloudFrontViewerCountry = cloudFrontViewerCountry;
    }
    public String getHost() {
        return host;
    }
    public void setHost(String host) {
        this.host = host;
    }
    public String getUpgradeInsecureRequests() {
        return upgradeInsecureRequests;
    }
    public void setUpgradeInsecureRequests(String upgradeInsecureRequests) {
        this.upgradeInsecureRequests = upgradeInsecureRequests;
    }
    public String getUserAgent() {
        return userAgent;
    }
    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }
    public String getVia() {
        return via;
    }
    public void setVia(String via) {
        this.via = via;
    }
    public String getXAmzCfId() {
        return xAmzCfId;
    }
    public void setXAmzCfId(String xAmzCfId) {
        this.xAmzCfId = xAmzCfId;
    }
    public String getXForwardedFor() {
        return xForwardedFor;
    }
    public void setXForwardedFor(String xForwardedFor) {
        this.xForwardedFor = xForwardedFor;
    }
    public String getXForwardedPort() {
        return xForwardedPort;
    }
    public void setXForwardedPort(String xForwardedPort) {
        this.xForwardedPort = xForwardedPort;
    }
    public String getXForwardedProto() {
        return xForwardedProto;
    }
    public void setXForwardedProto(String xForwardedProto) {
        this.xForwardedProto = xForwardedProto;
    }
}