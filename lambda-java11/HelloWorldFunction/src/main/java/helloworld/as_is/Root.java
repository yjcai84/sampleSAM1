
package helloworld.as_is;

public class Root {

    private String event;
    private Long eventId;
    private Long utcTimestampMs;
    private Long serverUtcTimestampMs;
    private Properties properties;
    public String getEvent() {
        return event;
    }
    public void setEvent(String event) {
        this.event = event;
    }
    public Long getEventId() {
        return eventId;
    }
    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }
    public Long getUtcTimestampMs() {
        return utcTimestampMs;
    }
    public void setUtcTimestampMs(Long utcTimestampMs) {
        this.utcTimestampMs = utcTimestampMs;
    }
    public Long getServerUtcTimestampMs() {
        return serverUtcTimestampMs;
    }
    public void setServerUtcTimestampMs(Long serverUtcTimestampMs) {
        this.serverUtcTimestampMs = serverUtcTimestampMs;
    }
    public Properties getProperties() {
        return properties;
    }
    public void setProperties(Properties properties) {
        this.properties = properties;
    }
}
