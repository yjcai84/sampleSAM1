
package helloworld.as_is;
public class Identity {
    private Object cognitoIdentityPoolId;
    private Object accountId;
    private Object cognitoIdentityId;
    private Object caller;
    private Object accessKey;
    private String sourceIp;
    private Object cognitoAuthenticationType;
    private Object cognitoAuthenticationProvider;
    private Object userArn;
    private String userAgent;
    private Object user;
    public Object getCognitoIdentityPoolId() {
        return cognitoIdentityPoolId;
    }
    public void setCognitoIdentityPoolId(Object cognitoIdentityPoolId) {
        this.cognitoIdentityPoolId = cognitoIdentityPoolId;
    }
    public Object getAccountId() {
        return accountId;
    }
    public void setAccountId(Object accountId) {
        this.accountId = accountId;
    }
    public Object getCognitoIdentityId() {
        return cognitoIdentityId;
    }
    public void setCognitoIdentityId(Object cognitoIdentityId) {
        this.cognitoIdentityId = cognitoIdentityId;
    }
    public Object getCaller() {
        return caller;
    }
    public void setCaller(Object caller) {
        this.caller = caller;
    }
    public Object getAccessKey() {
        return accessKey;
    }
    public void setAccessKey(Object accessKey) {
        this.accessKey = accessKey;
    }
    public String getSourceIp() {
        return sourceIp;
    }
    public void setSourceIp(String sourceIp) {
        this.sourceIp = sourceIp;
    }
    public Object getCognitoAuthenticationType() {
        return cognitoAuthenticationType;
    }
    public void setCognitoAuthenticationType(Object cognitoAuthenticationType) {
        this.cognitoAuthenticationType = cognitoAuthenticationType;
    }
    public Object getCognitoAuthenticationProvider() {
        return cognitoAuthenticationProvider;
    }
    public void setCognitoAuthenticationProvider(Object cognitoAuthenticationProvider) {
        this.cognitoAuthenticationProvider = cognitoAuthenticationProvider;
    }
    public Object getUserArn() {
        return userArn;
    }
    public void setUserArn(Object userArn) {
        this.userArn = userArn;
    }
    public String getUserAgent() {
        return userAgent;
    }
    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }
    public Object getUser() {
        return user;
    }
    public void setUser(Object user) {
        this.user = user;
    }
}