package helloworld.as_is;
import com.google.gson.annotations.SerializedName;

import helloworld.AllKeysRequired;
@AllKeysRequired
public class ThestralFeatures {
    private String enableCheckoutV2;
    private String enableCheckoutActionsV2;
    private String enableHolidayNav21;
    private String enableHalloweenNav;
    private String enableHomePageGate;
    private String enableHomepageImageSoftGate;
    private String infraCartUseSlsFirebaseWritesEnabled;
    private String enableSeasonalLogo;
    private String enableWebPendingPoints;
    private String backendPurchaseEventEnabled;
    private String enableCoinbase;
    private String enableAutoSavePdpAtc;
    private String showAutoSaveNewTag;
    private String autoSaveCategoryFilter;
    private String uniqueCartVariantFvfmEnabled;
    private String enableAutoSavePleEntry;
    private String enableBoxedUpSku;
    private String hideAccountInviteMobile;
    private String webPostOrderAttach;
    private String showGlobalPromotionsForBusinessUsers;
    private String enableProductPageGate;
    private String enableCategoryPageGate;
    private String enableCartV3;
    private String autoSaveSubscriptionsGallery;
    private String enableAutoSaveShipNow;
    private String enableLoyaltyFeatures;
    private String iosTrackUserUsage;
    private String syntheticInventoryFromPLM;
    private String iosPostOrderAttach;
    private String enableDirectPLM;
    private String upNonMemberLandingPageTest;
    private String mobilePostOrderAttach;
    private String purchaseVariantEvent;
    private String cashRewardsMessage;
    private String enablePLEV2;
    private String enableProductListV2;
    private String dataDome;
    private String sanathTest;
    private String apruveOauthEnabled;
    private String boxedUpCashRewards;
    private String dynamicInventory;
    private String boxedUpMaxCartQuantity;
    private String enableStateShoppingBan;
    private String enableExitSurvey;
    private String enableExitIntent;
    private String fastlyNextDayDelivery;
    private String newOrderDetailsEnabled;
    private String automationTestThestral14;
    private String fastlyExpressWarehouseForState;
    private String fastlyFindAllWarehouse;
    private String fastlyFindAllOrders;
    private String fastlyFindByClientType;
    private String fastlyFindAllProductListEntities;
    private String fastlyVariantsGetExpirationDate;
    private String enableExitNewUser;
    private String enableShippingFeesTest;
    private String categoryLite;
    private String donationEnabled;
    private String enablePostAddToCart;
    private String emptycartad;
    private String standardCategoryGroup;
    private String personalizedSortingForCoupons;
    private String personalizedAdsPDP;
    private String excludeRedeemedCouponsFromClientResponse;
    private String gateTest;
    private String personalizedAdsHomepage;
    private String enableLeaveCheckoutSearch;
    private String enableSampleUpsell;
    private String disableMobileAppleSignIn;
    private String enableCoupons;
    private String businessAccountPermissions;
    private String cacheBsTracking;
    private String prunedPriceModifiers;
    private String inviteCodeUsageEnabled;
    private String enableCheckoutTotalAlert;
    private String attachCouponIdToCartVariants;
    private String enablePersonalizedModule;
    private String dynamicFulfillment;
    private String switchToCartRecommendation;
    private String plmPricing;
    @SerializedName("holdenDatadogTest")
    private String holdenDatadogTest;
    @SerializedName("Holden test")
    private String holdenTest;
    private String dynamicInventoryTEST;
    private String silentCartVerify;
    private String useTestRec;
    private String enableCartBuilder;
    private String enableSurchargeSiteMessage;
    private String debounceCartRefresh;
    private String showHomeModuleRedesign;
    private String disableGeolocation;
    private String deliveryDelay;
    private String homepageCache;
    private String loggedInFlagTest;
    private String luckyCustomerSession;
    private String showCovidExpressMessaging;
    private String homeSectionTestMobile;
    private String multiFulfiller;
    private String strictPasswordRequirementsWeb;
    private String enableBoxedSubscriptions;
    private String pdpDeliveryMessaging;
    private String checkoutV2;
    private String showReplenishments;
    private String pleVariantTest;
    private String promoVariantCart;
    private String shippingMiddlewareCache;
    private String emailRollout;
    private String extensions;
    private String traceCartCtrlChange;
    private String shopTab;
    private String iosFilters;
    private String yourItems;
    private String pnPriming;
    private String standardCategoryGroupMobile;
    private String colourChange;
    private String reorderSort;
    private String iosCart;
    private String plesAutoComplete;
    private String plesMicroservice;
    private String mastercardWorldEliteEnabled;
    private String navRestructure;
    private String ftueVid;
    private String showInterestWordAlert;
    private String showComplementVariants;
    private String groupOrder;
    private String amexCheckout;
    private String brandFilter;
    private String boxedPlus;
    private String payItForward;
    private String smartCart;
    private String affinityItems;
    private String dallas;
    private String newFTUEAndroid;
    private String boxTops;
    private String augmentedReality;
    private String googlePay;
    private String googleSignIn;
    private String featuredTab;
    private String pdpRefresh;
    private String homeV2;
    @SerializedName("UPMobileCart")
    private String uPMobileCart;
    private String cartResolverPlacement;
    private String disableRegistrationEmails;
    private String dynamicFulfillmentPricing;
    private String upCatBanner;
    @SerializedName("onSale")
    private String onSale;
    public String getOnSale() {
        return onSale;
    }
    public void setOnSale(String onSale) {
        this.onSale = onSale;
    }
    public String getEnableCheckoutV2() {
        return enableCheckoutV2;
    }
    public void setEnableCheckoutV2(String enableCheckoutV2) {
        this.enableCheckoutV2 = enableCheckoutV2;
    }
    public String getEnableCheckoutActionsV2() {
        return enableCheckoutActionsV2;
    }
    public void setEnableCheckoutActionsV2(String enableCheckoutActionsV2) {
        this.enableCheckoutActionsV2 = enableCheckoutActionsV2;
    }
    public String getEnableHolidayNav21() {
        return enableHolidayNav21;
    }
    public void setEnableHolidayNav21(String enableHolidayNav21) {
        this.enableHolidayNav21 = enableHolidayNav21;
    }
    public String getEnableHalloweenNav() {
        return enableHalloweenNav;
    }
    public void setEnableHalloweenNav(String enableHalloweenNav) {
        this.enableHalloweenNav = enableHalloweenNav;
    }
    public String getEnableHomePageGate() {
        return enableHomePageGate;
    }
    public void setEnableHomePageGate(String enableHomePageGate) {
        this.enableHomePageGate = enableHomePageGate;
    }
    public String getEnableHomepageImageSoftGate() {
        return enableHomepageImageSoftGate;
    }
    public void setEnableHomepageImageSoftGate(String enableHomepageImageSoftGate) {
        this.enableHomepageImageSoftGate = enableHomepageImageSoftGate;
    }
    public String getInfraCartUseSlsFirebaseWritesEnabled() {
        return infraCartUseSlsFirebaseWritesEnabled;
    }
    public void setInfraCartUseSlsFirebaseWritesEnabled(String infraCartUseSlsFirebaseWritesEnabled) {
        this.infraCartUseSlsFirebaseWritesEnabled = infraCartUseSlsFirebaseWritesEnabled;
    }
    public String getEnableSeasonalLogo() {
        return enableSeasonalLogo;
    }
    public void setEnableSeasonalLogo(String enableSeasonalLogo) {
        this.enableSeasonalLogo = enableSeasonalLogo;
    }
    public String getEnableWebPendingPoints() {
        return enableWebPendingPoints;
    }
    public void setEnableWebPendingPoints(String enableWebPendingPoints) {
        this.enableWebPendingPoints = enableWebPendingPoints;
    }
    public String getBackendPurchaseEventEnabled() {
        return backendPurchaseEventEnabled;
    }
    public void setBackendPurchaseEventEnabled(String backendPurchaseEventEnabled) {
        this.backendPurchaseEventEnabled = backendPurchaseEventEnabled;
    }
    public String getEnableCoinbase() {
        return enableCoinbase;
    }
    public void setEnableCoinbase(String enableCoinbase) {
        this.enableCoinbase = enableCoinbase;
    }
    public String getEnableAutoSavePdpAtc() {
        return enableAutoSavePdpAtc;
    }
    public void setEnableAutoSavePdpAtc(String enableAutoSavePdpAtc) {
        this.enableAutoSavePdpAtc = enableAutoSavePdpAtc;
    }
    public String getShowAutoSaveNewTag() {
        return showAutoSaveNewTag;
    }
    public void setShowAutoSaveNewTag(String showAutoSaveNewTag) {
        this.showAutoSaveNewTag = showAutoSaveNewTag;
    }
    public String getAutoSaveCategoryFilter() {
        return autoSaveCategoryFilter;
    }
    public void setAutoSaveCategoryFilter(String autoSaveCategoryFilter) {
        this.autoSaveCategoryFilter = autoSaveCategoryFilter;
    }
    public String getUniqueCartVariantFvfmEnabled() {
        return uniqueCartVariantFvfmEnabled;
    }
    public void setUniqueCartVariantFvfmEnabled(String uniqueCartVariantFvfmEnabled) {
        this.uniqueCartVariantFvfmEnabled = uniqueCartVariantFvfmEnabled;
    }
    public String getEnableAutoSavePleEntry() {
        return enableAutoSavePleEntry;
    }
    public void setEnableAutoSavePleEntry(String enableAutoSavePleEntry) {
        this.enableAutoSavePleEntry = enableAutoSavePleEntry;
    }
    public String getEnableBoxedUpSku() {
        return enableBoxedUpSku;
    }
    public void setEnableBoxedUpSku(String enableBoxedUpSku) {
        this.enableBoxedUpSku = enableBoxedUpSku;
    }
    public String getHideAccountInviteMobile() {
        return hideAccountInviteMobile;
    }
    public void setHideAccountInviteMobile(String hideAccountInviteMobile) {
        this.hideAccountInviteMobile = hideAccountInviteMobile;
    }
    public String getWebPostOrderAttach() {
        return webPostOrderAttach;
    }
    public void setWebPostOrderAttach(String webPostOrderAttach) {
        this.webPostOrderAttach = webPostOrderAttach;
    }
    public String getShowGlobalPromotionsForBusinessUsers() {
        return showGlobalPromotionsForBusinessUsers;
    }
    public void setShowGlobalPromotionsForBusinessUsers(String showGlobalPromotionsForBusinessUsers) {
        this.showGlobalPromotionsForBusinessUsers = showGlobalPromotionsForBusinessUsers;
    }
    public String getEnableProductPageGate() {
        return enableProductPageGate;
    }
    public void setEnableProductPageGate(String enableProductPageGate) {
        this.enableProductPageGate = enableProductPageGate;
    }
    public String getEnableCategoryPageGate() {
        return enableCategoryPageGate;
    }
    public void setEnableCategoryPageGate(String enableCategoryPageGate) {
        this.enableCategoryPageGate = enableCategoryPageGate;
    }
    public String getEnableCartV3() {
        return enableCartV3;
    }
    public void setEnableCartV3(String enableCartV3) {
        this.enableCartV3 = enableCartV3;
    }
    public String getAutoSaveSubscriptionsGallery() {
        return autoSaveSubscriptionsGallery;
    }
    public void setAutoSaveSubscriptionsGallery(String autoSaveSubscriptionsGallery) {
        this.autoSaveSubscriptionsGallery = autoSaveSubscriptionsGallery;
    }
    public String getEnableAutoSaveShipNow() {
        return enableAutoSaveShipNow;
    }
    public void setEnableAutoSaveShipNow(String enableAutoSaveShipNow) {
        this.enableAutoSaveShipNow = enableAutoSaveShipNow;
    }
    public String getEnableLoyaltyFeatures() {
        return enableLoyaltyFeatures;
    }
    public void setEnableLoyaltyFeatures(String enableLoyaltyFeatures) {
        this.enableLoyaltyFeatures = enableLoyaltyFeatures;
    }
    public String getIosTrackUserUsage() {
        return iosTrackUserUsage;
    }
    public void setIosTrackUserUsage(String iosTrackUserUsage) {
        this.iosTrackUserUsage = iosTrackUserUsage;
    }
    public String getSyntheticInventoryFromPLM() {
        return syntheticInventoryFromPLM;
    }
    public void setSyntheticInventoryFromPLM(String syntheticInventoryFromPLM) {
        this.syntheticInventoryFromPLM = syntheticInventoryFromPLM;
    }
    public String getIosPostOrderAttach() {
        return iosPostOrderAttach;
    }
    public void setIosPostOrderAttach(String iosPostOrderAttach) {
        this.iosPostOrderAttach = iosPostOrderAttach;
    }
    public String getEnableDirectPLM() {
        return enableDirectPLM;
    }
    public void setEnableDirectPLM(String enableDirectPLM) {
        this.enableDirectPLM = enableDirectPLM;
    }
    public String getUpNonMemberLandingPageTest() {
        return upNonMemberLandingPageTest;
    }
    public void setUpNonMemberLandingPageTest(String upNonMemberLandingPageTest) {
        this.upNonMemberLandingPageTest = upNonMemberLandingPageTest;
    }
    public String getMobilePostOrderAttach() {
        return mobilePostOrderAttach;
    }
    public void setMobilePostOrderAttach(String mobilePostOrderAttach) {
        this.mobilePostOrderAttach = mobilePostOrderAttach;
    }
    public String getPurchaseVariantEvent() {
        return purchaseVariantEvent;
    }
    public void setPurchaseVariantEvent(String purchaseVariantEvent) {
        this.purchaseVariantEvent = purchaseVariantEvent;
    }
    public String getCashRewardsMessage() {
        return cashRewardsMessage;
    }
    public void setCashRewardsMessage(String cashRewardsMessage) {
        this.cashRewardsMessage = cashRewardsMessage;
    }
    public String getEnablePLEV2() {
        return enablePLEV2;
    }
    public void setEnablePLEV2(String enablePLEV2) {
        this.enablePLEV2 = enablePLEV2;
    }
    public String getEnableProductListV2() {
        return enableProductListV2;
    }
    public void setEnableProductListV2(String enableProductListV2) {
        this.enableProductListV2 = enableProductListV2;
    }
    public String getDataDome() {
        return dataDome;
    }
    public void setDataDome(String dataDome) {
        this.dataDome = dataDome;
    }
    public String getSanathTest() {
        return sanathTest;
    }
    public void setSanathTest(String sanathTest) {
        this.sanathTest = sanathTest;
    }
    public String getApruveOauthEnabled() {
        return apruveOauthEnabled;
    }
    public void setApruveOauthEnabled(String apruveOauthEnabled) {
        this.apruveOauthEnabled = apruveOauthEnabled;
    }
    public String getBoxedUpCashRewards() {
        return boxedUpCashRewards;
    }
    public void setBoxedUpCashRewards(String boxedUpCashRewards) {
        this.boxedUpCashRewards = boxedUpCashRewards;
    }
    public String getDynamicInventory() {
        return dynamicInventory;
    }
    public void setDynamicInventory(String dynamicInventory) {
        this.dynamicInventory = dynamicInventory;
    }
    public String getBoxedUpMaxCartQuantity() {
        return boxedUpMaxCartQuantity;
    }
    public void setBoxedUpMaxCartQuantity(String boxedUpMaxCartQuantity) {
        this.boxedUpMaxCartQuantity = boxedUpMaxCartQuantity;
    }
    public String getEnableStateShoppingBan() {
        return enableStateShoppingBan;
    }
    public void setEnableStateShoppingBan(String enableStateShoppingBan) {
        this.enableStateShoppingBan = enableStateShoppingBan;
    }
    public String getEnableExitSurvey() {
        return enableExitSurvey;
    }
    public void setEnableExitSurvey(String enableExitSurvey) {
        this.enableExitSurvey = enableExitSurvey;
    }
    public String getEnableExitIntent() {
        return enableExitIntent;
    }
    public void setEnableExitIntent(String enableExitIntent) {
        this.enableExitIntent = enableExitIntent;
    }
    public String getFastlyNextDayDelivery() {
        return fastlyNextDayDelivery;
    }
    public void setFastlyNextDayDelivery(String fastlyNextDayDelivery) {
        this.fastlyNextDayDelivery = fastlyNextDayDelivery;
    }
    public String getNewOrderDetailsEnabled() {
        return newOrderDetailsEnabled;
    }
    public void setNewOrderDetailsEnabled(String newOrderDetailsEnabled) {
        this.newOrderDetailsEnabled = newOrderDetailsEnabled;
    }
    public String getAutomationTestThestral14() {
        return automationTestThestral14;
    }
    public void setAutomationTestThestral14(String automationTestThestral14) {
        this.automationTestThestral14 = automationTestThestral14;
    }
    public String getFastlyExpressWarehouseForState() {
        return fastlyExpressWarehouseForState;
    }
    public void setFastlyExpressWarehouseForState(String fastlyExpressWarehouseForState) {
        this.fastlyExpressWarehouseForState = fastlyExpressWarehouseForState;
    }
    public String getFastlyFindAllWarehouse() {
        return fastlyFindAllWarehouse;
    }
    public void setFastlyFindAllWarehouse(String fastlyFindAllWarehouse) {
        this.fastlyFindAllWarehouse = fastlyFindAllWarehouse;
    }
    public String getFastlyFindAllOrders() {
        return fastlyFindAllOrders;
    }
    public void setFastlyFindAllOrders(String fastlyFindAllOrders) {
        this.fastlyFindAllOrders = fastlyFindAllOrders;
    }
    public String getFastlyFindByClientType() {
        return fastlyFindByClientType;
    }
    public void setFastlyFindByClientType(String fastlyFindByClientType) {
        this.fastlyFindByClientType = fastlyFindByClientType;
    }
    public String getFastlyFindAllProductListEntities() {
        return fastlyFindAllProductListEntities;
    }
    public void setFastlyFindAllProductListEntities(String fastlyFindAllProductListEntities) {
        this.fastlyFindAllProductListEntities = fastlyFindAllProductListEntities;
    }
    public String getFastlyVariantsGetExpirationDate() {
        return fastlyVariantsGetExpirationDate;
    }
    public void setFastlyVariantsGetExpirationDate(String fastlyVariantsGetExpirationDate) {
        this.fastlyVariantsGetExpirationDate = fastlyVariantsGetExpirationDate;
    }
    public String getEnableExitNewUser() {
        return enableExitNewUser;
    }
    public void setEnableExitNewUser(String enableExitNewUser) {
        this.enableExitNewUser = enableExitNewUser;
    }
    public String getEnableShippingFeesTest() {
        return enableShippingFeesTest;
    }
    public void setEnableShippingFeesTest(String enableShippingFeesTest) {
        this.enableShippingFeesTest = enableShippingFeesTest;
    }
    public String getCategoryLite() {
        return categoryLite;
    }
    public void setCategoryLite(String categoryLite) {
        this.categoryLite = categoryLite;
    }
    public String getDonationEnabled() {
        return donationEnabled;
    }
    public void setDonationEnabled(String donationEnabled) {
        this.donationEnabled = donationEnabled;
    }
    public String getEnablePostAddToCart() {
        return enablePostAddToCart;
    }
    public void setEnablePostAddToCart(String enablePostAddToCart) {
        this.enablePostAddToCart = enablePostAddToCart;
    }
    public String getEmptycartad() {
        return emptycartad;
    }
    public void setEmptycartad(String emptycartad) {
        this.emptycartad = emptycartad;
    }
    public String getStandardCategoryGroup() {
        return standardCategoryGroup;
    }
    public void setStandardCategoryGroup(String standardCategoryGroup) {
        this.standardCategoryGroup = standardCategoryGroup;
    }
    public String getPersonalizedSortingForCoupons() {
        return personalizedSortingForCoupons;
    }
    public void setPersonalizedSortingForCoupons(String personalizedSortingForCoupons) {
        this.personalizedSortingForCoupons = personalizedSortingForCoupons;
    }
    public String getPersonalizedAdsPDP() {
        return personalizedAdsPDP;
    }
    public void setPersonalizedAdsPDP(String personalizedAdsPDP) {
        this.personalizedAdsPDP = personalizedAdsPDP;
    }
    public String getExcludeRedeemedCouponsFromClientResponse() {
        return excludeRedeemedCouponsFromClientResponse;
    }
    public void setExcludeRedeemedCouponsFromClientResponse(String excludeRedeemedCouponsFromClientResponse) {
        this.excludeRedeemedCouponsFromClientResponse = excludeRedeemedCouponsFromClientResponse;
    }
    public String getGateTest() {
        return gateTest;
    }
    public void setGateTest(String gateTest) {
        this.gateTest = gateTest;
    }
    public String getPersonalizedAdsHomepage() {
        return personalizedAdsHomepage;
    }
    public void setPersonalizedAdsHomepage(String personalizedAdsHomepage) {
        this.personalizedAdsHomepage = personalizedAdsHomepage;
    }
    public String getEnableLeaveCheckoutSearch() {
        return enableLeaveCheckoutSearch;
    }
    public void setEnableLeaveCheckoutSearch(String enableLeaveCheckoutSearch) {
        this.enableLeaveCheckoutSearch = enableLeaveCheckoutSearch;
    }
    public String getEnableSampleUpsell() {
        return enableSampleUpsell;
    }
    public void setEnableSampleUpsell(String enableSampleUpsell) {
        this.enableSampleUpsell = enableSampleUpsell;
    }
    public String getDisableMobileAppleSignIn() {
        return disableMobileAppleSignIn;
    }
    public void setDisableMobileAppleSignIn(String disableMobileAppleSignIn) {
        this.disableMobileAppleSignIn = disableMobileAppleSignIn;
    }
    public String getEnableCoupons() {
        return enableCoupons;
    }
    public void setEnableCoupons(String enableCoupons) {
        this.enableCoupons = enableCoupons;
    }
    public String getBusinessAccountPermissions() {
        return businessAccountPermissions;
    }
    public void setBusinessAccountPermissions(String businessAccountPermissions) {
        this.businessAccountPermissions = businessAccountPermissions;
    }
    public String getCacheBsTracking() {
        return cacheBsTracking;
    }
    public void setCacheBsTracking(String cacheBsTracking) {
        this.cacheBsTracking = cacheBsTracking;
    }
    public String getPrunedPriceModifiers() {
        return prunedPriceModifiers;
    }
    public void setPrunedPriceModifiers(String prunedPriceModifiers) {
        this.prunedPriceModifiers = prunedPriceModifiers;
    }
    public String getInviteCodeUsageEnabled() {
        return inviteCodeUsageEnabled;
    }
    public void setInviteCodeUsageEnabled(String inviteCodeUsageEnabled) {
        this.inviteCodeUsageEnabled = inviteCodeUsageEnabled;
    }
    public String getEnableCheckoutTotalAlert() {
        return enableCheckoutTotalAlert;
    }
    public void setEnableCheckoutTotalAlert(String enableCheckoutTotalAlert) {
        this.enableCheckoutTotalAlert = enableCheckoutTotalAlert;
    }
    public String getAttachCouponIdToCartVariants() {
        return attachCouponIdToCartVariants;
    }
    public void setAttachCouponIdToCartVariants(String attachCouponIdToCartVariants) {
        this.attachCouponIdToCartVariants = attachCouponIdToCartVariants;
    }
    public String getEnablePersonalizedModule() {
        return enablePersonalizedModule;
    }
    public void setEnablePersonalizedModule(String enablePersonalizedModule) {
        this.enablePersonalizedModule = enablePersonalizedModule;
    }
    public String getDynamicFulfillment() {
        return dynamicFulfillment;
    }
    public void setDynamicFulfillment(String dynamicFulfillment) {
        this.dynamicFulfillment = dynamicFulfillment;
    }
    public String getSwitchToCartRecommendation() {
        return switchToCartRecommendation;
    }
    public void setSwitchToCartRecommendation(String switchToCartRecommendation) {
        this.switchToCartRecommendation = switchToCartRecommendation;
    }
    public String getPlmPricing() {
        return plmPricing;
    }
    public void setPlmPricing(String plmPricing) {
        this.plmPricing = plmPricing;
    }
    public String getHoldenDatadogTest() {
        return holdenDatadogTest;
    }
    public void setHoldenDatadogTest(String holdenDatadogTest) {
        this.holdenDatadogTest = holdenDatadogTest;
    }
    public String getHoldenTest() {
        return holdenTest;
    }
    public void setHoldenTest(String holdenTest) {
        this.holdenTest = holdenTest;
    }
    public String getDynamicInventoryTEST() {
        return dynamicInventoryTEST;
    }
    public void setDynamicInventoryTEST(String dynamicInventoryTEST) {
        this.dynamicInventoryTEST = dynamicInventoryTEST;
    }
    public String getSilentCartVerify() {
        return silentCartVerify;
    }
    public void setSilentCartVerify(String silentCartVerify) {
        this.silentCartVerify = silentCartVerify;
    }
    public String getUseTestRec() {
        return useTestRec;
    }
    public void setUseTestRec(String useTestRec) {
        this.useTestRec = useTestRec;
    }
    public String getEnableCartBuilder() {
        return enableCartBuilder;
    }
    public void setEnableCartBuilder(String enableCartBuilder) {
        this.enableCartBuilder = enableCartBuilder;
    }
    public String getEnableSurchargeSiteMessage() {
        return enableSurchargeSiteMessage;
    }
    public void setEnableSurchargeSiteMessage(String enableSurchargeSiteMessage) {
        this.enableSurchargeSiteMessage = enableSurchargeSiteMessage;
    }
    public String getDebounceCartRefresh() {
        return debounceCartRefresh;
    }
    public void setDebounceCartRefresh(String debounceCartRefresh) {
        this.debounceCartRefresh = debounceCartRefresh;
    }
    public String getShowHomeModuleRedesign() {
        return showHomeModuleRedesign;
    }
    public void setShowHomeModuleRedesign(String showHomeModuleRedesign) {
        this.showHomeModuleRedesign = showHomeModuleRedesign;
    }
    public String getDisableGeolocation() {
        return disableGeolocation;
    }
    public void setDisableGeolocation(String disableGeolocation) {
        this.disableGeolocation = disableGeolocation;
    }
    public String getDeliveryDelay() {
        return deliveryDelay;
    }
    public void setDeliveryDelay(String deliveryDelay) {
        this.deliveryDelay = deliveryDelay;
    }
    public String getHomepageCache() {
        return homepageCache;
    }
    public void setHomepageCache(String homepageCache) {
        this.homepageCache = homepageCache;
    }
    public String getLoggedInFlagTest() {
        return loggedInFlagTest;
    }
    public void setLoggedInFlagTest(String loggedInFlagTest) {
        this.loggedInFlagTest = loggedInFlagTest;
    }
    public String getLuckyCustomerSession() {
        return luckyCustomerSession;
    }
    public void setLuckyCustomerSession(String luckyCustomerSession) {
        this.luckyCustomerSession = luckyCustomerSession;
    }
    public String getShowCovidExpressMessaging() {
        return showCovidExpressMessaging;
    }
    public void setShowCovidExpressMessaging(String showCovidExpressMessaging) {
        this.showCovidExpressMessaging = showCovidExpressMessaging;
    }
    public String getHomeSectionTestMobile() {
        return homeSectionTestMobile;
    }
    public void setHomeSectionTestMobile(String homeSectionTestMobile) {
        this.homeSectionTestMobile = homeSectionTestMobile;
    }
    public String getMultiFulfiller() {
        return multiFulfiller;
    }
    public void setMultiFulfiller(String multiFulfiller) {
        this.multiFulfiller = multiFulfiller;
    }
    public String getStrictPasswordRequirementsWeb() {
        return strictPasswordRequirementsWeb;
    }
    public void setStrictPasswordRequirementsWeb(String strictPasswordRequirementsWeb) {
        this.strictPasswordRequirementsWeb = strictPasswordRequirementsWeb;
    }
    public String getEnableBoxedSubscriptions() {
        return enableBoxedSubscriptions;
    }
    public void setEnableBoxedSubscriptions(String enableBoxedSubscriptions) {
        this.enableBoxedSubscriptions = enableBoxedSubscriptions;
    }
    public String getPdpDeliveryMessaging() {
        return pdpDeliveryMessaging;
    }
    public void setPdpDeliveryMessaging(String pdpDeliveryMessaging) {
        this.pdpDeliveryMessaging = pdpDeliveryMessaging;
    }
    public String getCheckoutV2() {
        return checkoutV2;
    }
    public void setCheckoutV2(String checkoutV2) {
        this.checkoutV2 = checkoutV2;
    }
    public String getShowReplenishments() {
        return showReplenishments;
    }
    public void setShowReplenishments(String showReplenishments) {
        this.showReplenishments = showReplenishments;
    }
    public String getPleVariantTest() {
        return pleVariantTest;
    }
    public void setPleVariantTest(String pleVariantTest) {
        this.pleVariantTest = pleVariantTest;
    }
    public String getPromoVariantCart() {
        return promoVariantCart;
    }
    public void setPromoVariantCart(String promoVariantCart) {
        this.promoVariantCart = promoVariantCart;
    }
    public String getShippingMiddlewareCache() {
        return shippingMiddlewareCache;
    }
    public void setShippingMiddlewareCache(String shippingMiddlewareCache) {
        this.shippingMiddlewareCache = shippingMiddlewareCache;
    }
    public String getEmailRollout() {
        return emailRollout;
    }
    public void setEmailRollout(String emailRollout) {
        this.emailRollout = emailRollout;
    }
    public String getExtensions() {
        return extensions;
    }
    public void setExtensions(String extensions) {
        this.extensions = extensions;
    }
    public String getTraceCartCtrlChange() {
        return traceCartCtrlChange;
    }
    public void setTraceCartCtrlChange(String traceCartCtrlChange) {
        this.traceCartCtrlChange = traceCartCtrlChange;
    }
    public String getShopTab() {
        return shopTab;
    }
    public void setShopTab(String shopTab) {
        this.shopTab = shopTab;
    }
    public String getIosFilters() {
        return iosFilters;
    }
    public void setIosFilters(String iosFilters) {
        this.iosFilters = iosFilters;
    }
    public String getYourItems() {
        return yourItems;
    }
    public void setYourItems(String yourItems) {
        this.yourItems = yourItems;
    }
    public String getPnPriming() {
        return pnPriming;
    }
    public void setPnPriming(String pnPriming) {
        this.pnPriming = pnPriming;
    }
    public String getStandardCategoryGroupMobile() {
        return standardCategoryGroupMobile;
    }
    public void setStandardCategoryGroupMobile(String standardCategoryGroupMobile) {
        this.standardCategoryGroupMobile = standardCategoryGroupMobile;
    }
    public String getColourChange() {
        return colourChange;
    }
    public void setColourChange(String colourChange) {
        this.colourChange = colourChange;
    }
    public String getReorderSort() {
        return reorderSort;
    }
    public void setReorderSort(String reorderSort) {
        this.reorderSort = reorderSort;
    }
    public String getIosCart() {
        return iosCart;
    }
    public void setIosCart(String iosCart) {
        this.iosCart = iosCart;
    }
    public String getPlesAutoComplete() {
        return plesAutoComplete;
    }
    public void setPlesAutoComplete(String plesAutoComplete) {
        this.plesAutoComplete = plesAutoComplete;
    }
    public String getPlesMicroservice() {
        return plesMicroservice;
    }
    public void setPlesMicroservice(String plesMicroservice) {
        this.plesMicroservice = plesMicroservice;
    }
    public String getMastercardWorldEliteEnabled() {
        return mastercardWorldEliteEnabled;
    }
    public void setMastercardWorldEliteEnabled(String mastercardWorldEliteEnabled) {
        this.mastercardWorldEliteEnabled = mastercardWorldEliteEnabled;
    }
    public String getNavRestructure() {
        return navRestructure;
    }
    public void setNavRestructure(String navRestructure) {
        this.navRestructure = navRestructure;
    }
    public String getFtueVid() {
        return ftueVid;
    }
    public void setFtueVid(String ftueVid) {
        this.ftueVid = ftueVid;
    }
    public String getShowInterestWordAlert() {
        return showInterestWordAlert;
    }
    public void setShowInterestWordAlert(String showInterestWordAlert) {
        this.showInterestWordAlert = showInterestWordAlert;
    }
    public String getShowComplementVariants() {
        return showComplementVariants;
    }
    public void setShowComplementVariants(String showComplementVariants) {
        this.showComplementVariants = showComplementVariants;
    }
    public String getGroupOrder() {
        return groupOrder;
    }
    public void setGroupOrder(String groupOrder) {
        this.groupOrder = groupOrder;
    }
    public String getAmexCheckout() {
        return amexCheckout;
    }
    public void setAmexCheckout(String amexCheckout) {
        this.amexCheckout = amexCheckout;
    }
    public String getBrandFilter() {
        return brandFilter;
    }
    public void setBrandFilter(String brandFilter) {
        this.brandFilter = brandFilter;
    }
    public String getBoxedPlus() {
        return boxedPlus;
    }
    public void setBoxedPlus(String boxedPlus) {
        this.boxedPlus = boxedPlus;
    }
    public String getPayItForward() {
        return payItForward;
    }
    public void setPayItForward(String payItForward) {
        this.payItForward = payItForward;
    }
    public String getSmartCart() {
        return smartCart;
    }
    public void setSmartCart(String smartCart) {
        this.smartCart = smartCart;
    }
    public String getAffinityItems() {
        return affinityItems;
    }
    public void setAffinityItems(String affinityItems) {
        this.affinityItems = affinityItems;
    }
    public String getDallas() {
        return dallas;
    }
    public void setDallas(String dallas) {
        this.dallas = dallas;
    }
    public String getNewFTUEAndroid() {
        return newFTUEAndroid;
    }
    public void setNewFTUEAndroid(String newFTUEAndroid) {
        this.newFTUEAndroid = newFTUEAndroid;
    }
    public String getBoxTops() {
        return boxTops;
    }
    public void setBoxTops(String boxTops) {
        this.boxTops = boxTops;
    }
    public String getAugmentedReality() {
        return augmentedReality;
    }
    public void setAugmentedReality(String augmentedReality) {
        this.augmentedReality = augmentedReality;
    }
    public String getGooglePay() {
        return googlePay;
    }
    public void setGooglePay(String googlePay) {
        this.googlePay = googlePay;
    }
    public String getGoogleSignIn() {
        return googleSignIn;
    }
    public void setGoogleSignIn(String googleSignIn) {
        this.googleSignIn = googleSignIn;
    }
    public String getFeaturedTab() {
        return featuredTab;
    }
    public void setFeaturedTab(String featuredTab) {
        this.featuredTab = featuredTab;
    }
    public String getPdpRefresh() {
        return pdpRefresh;
    }
    public void setPdpRefresh(String pdpRefresh) {
        this.pdpRefresh = pdpRefresh;
    }
    public String getHomeV2() {
        return homeV2;
    }
    public void setHomeV2(String homeV2) {
        this.homeV2 = homeV2;
    }
    public String getUPMobileCart() {
        return uPMobileCart;
    }
    public void setUPMobileCart(String uPMobileCart) {
        this.uPMobileCart = uPMobileCart;
    }
    public String getCartResolverPlacement() {
        return cartResolverPlacement;
    }
    public void setCartResolverPlacement(String cartResolverPlacement) {
        this.cartResolverPlacement = cartResolverPlacement;
    }
    public String getDisableRegistrationEmails() {
        return disableRegistrationEmails;
    }
    public void setDisableRegistrationEmails(String disableRegistrationEmails) {
        this.disableRegistrationEmails = disableRegistrationEmails;
    }
    public String getDynamicFulfillmentPricing() {
        return dynamicFulfillmentPricing;
    }
    public void setDynamicFulfillmentPricing(String dynamicFulfillmentPricing) {
        this.dynamicFulfillmentPricing = dynamicFulfillmentPricing;
    }
    public String getUpCatBanner() {
        return upCatBanner;
    }
    public void setUpCatBanner(String upCatBanner) {
        this.upCatBanner = upCatBanner;
    }
}
