package helloworld.as_is;
import helloworld.AllKeysRequired;
@AllKeysRequired
public class BsProperties {
    private String postalCode;
    private String expressWarehouseId;
    private String standardWarehouseId;
    public String getPostalCode() {
        return postalCode;
    }
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }
    public String getExpressWarehouseId() {
        return expressWarehouseId;
    }
    public void setExpressWarehouseId(String expressWarehouseId) {
        this.expressWarehouseId = expressWarehouseId;
    }
    public String getStandardWarehouseId() {
        return standardWarehouseId;
    }
    public void setStandardWarehouseId(String standardWarehouseId) {
        this.standardWarehouseId = standardWarehouseId;
    }
}