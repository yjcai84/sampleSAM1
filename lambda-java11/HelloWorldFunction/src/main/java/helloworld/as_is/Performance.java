package helloworld.as_is;

public class Performance {

    private float domReady;
    private float ttfb;
  
   // Getter Methods 
  
    public float getDomReady() {
      return domReady;
    }
  
    public float getTtfb() {
      return ttfb;
    }
  
   // Setter Methods 
  
    public void setDomReady( float domReady ) {
      this.domReady = domReady;
    }
  
    public void setTtfb( float ttfb ) {
      this.ttfb = ttfb;
    }
  }