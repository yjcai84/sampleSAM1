package helloworld.as_is;
import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

import helloworld.AllKeysRequired;
@AllKeysRequired
public class Properties {
    @SerializedName("userId")
    private String userId;
    @SerializedName("deviceId")
    private String deviceId;
    @SerializedName("thestralFeatures")
    private ThestralFeatures thestralFeatures;
    @SerializedName("hasCookiedEmail")
    private Boolean hasCookiedEmail;
    @SerializedName("appVersion")
    private String appVersion;
    @SerializedName("postalCode")
    private String postalCode;
    @SerializedName("utm_medium")
    private Object utmMedium;
    @SerializedName("userAgent.browser")
    private String userAgentBrowser;
    @SerializedName("utm_experiment")
    private Object utmExperiment;
    @SerializedName("vitalTypeDisplay")
    private String vitalTypeDisplay;
    @SerializedName("manufacturer")
    private String manufacturer;
    @SerializedName("userAgent.os")
    private String userAgentOs;
    @SerializedName("queryParameters")
    private QueryParameters queryParameters;
    @SerializedName("osVersion")
    private String osVersion;
    @SerializedName("userAgent.string")
    private String userAgentString;
    @SerializedName("utm_purpose")
    private Object utmPurpose;
    @SerializedName("utm_ad_id")
    private Object utmAdId;
    private String remoteAddress;
    @SerializedName("utm_content")
    private Object utmContent;
    @SerializedName("vitalValue")
    private Double vitalValue;
    @SerializedName("utm_target")
    private Object utmTarget;
    private String os;
    @SerializedName("utm_campaign")
    private Object utmCampaign;
    @SerializedName("webSlugConfigId")
    private String webSlugConfigId;
    @SerializedName("appRelease")
    private String appRelease;
    @SerializedName("hasPurchasedBefore")
    private String hasPurchasedBefore;
    @SerializedName("userAgent")
    private String userAgent;
    @SerializedName("sessionId")
    private String sessionId;
    @SerializedName("membershipStatus")
    private Long membershipStatus;
    @SerializedName("userAgent.device")
    private String userAgentDevice;
    @SerializedName("utm_term")
    private Object utmTerm;
    @SerializedName("currentCartSharedId")
    private Object currentCartSharedId;
    @SerializedName("isReprocessed")
    private Boolean isReprocessed;
    @SerializedName("bsProperties")
    private BsProperties bsProperties;
    @SerializedName("isLoggedIn")
    private Boolean isLoggedIn;
    @SerializedName("shouldAutoRenew")
    private Boolean shouldAutoRenew;
    @SerializedName("page")
    private String page;
    @SerializedName("libVersion")
    private String libVersion;
    @SerializedName("vitalType")
    private Long vitalType;
    @SerializedName("utm_source")
    private Object utmSource;
    @SerializedName("oldMembershipStatus")
    private Long oldMembershipStatus;
    @SerializedName("newMembershipStatus")
    private Long newMembershipStatus;
    @SerializedName("platform")
    private String platform;
    @SerializedName("apiVersion")
    private String apiVersion;
    @SerializedName("v")
    private String v;
    @SerializedName("setWillAutoRenewOff")
    private boolean setWillAutoRenewOff;
    @SerializedName("pdpIntent")
    private Long pdpIntent;
    @SerializedName("inStock")
    private boolean inStock;
    @SerializedName("variantId")
    private String variantId;
    @SerializedName("intentRelatedId")
    private String intentRelatedId;
    @SerializedName("pleIntent")
    private String pleIntent;
    @SerializedName("esNumResults")
    private String esNumResults;
    @SerializedName("query")
    private String query;
    @SerializedName("numResults")
    private Long numResults;
    @SerializedName("type")
    private String type;
    @SerializedName("esTime")
    private Long esTime;
    @SerializedName("rowPosition")
    private Long rowPosition;
    @SerializedName("intentType")
    private String intentType = null;
    @SerializedName("moduleTotal")
    private Long moduleTotal;
    @SerializedName("rowsTotal")
    private Long rowsTotal;
    @SerializedName("modulePosition")
    private Long modulePosition;
    @SerializedName("moduleType")
    private Long moduleType;
    @SerializedName("sourceScreenName")
    private String sourceScreenName;
    @SerializedName("columnModulePosition")
    private Long columnModulePosition;
    @SerializedName("sourceType")
    private Long sourceType;
    @SerializedName("columnPosition")
    private Long columnPosition;   
    @SerializedName("plePosition")
    private Long plePosition;
    @SerializedName("pleTotal")
    private Long pleTotal;
    @SerializedName("price")
    private Long price;
    @SerializedName("correlationId")
    private String correlationId;
    @SerializedName("originalDeviceId")
    private String originalDeviceId;
    @SerializedName("email")
    private String email;
    @SerializedName("cartVariantSubtotalValue")
    private float cartVariantSubtotalValue;
    @SerializedName("hasSampleItem")
    private boolean hasSampleItem;
    @SerializedName("obgibState")
    private float obgibState;
    @SerializedName("isFreeShippingPerkUser")
    private boolean isFreeShippingPerkUser;
    @SerializedName("obgibDeliveryDate")
    private String obgibDeliveryDate = null;
    @SerializedName("isFreeShippingNoLimit")
    private boolean isFreeShippingNoLimit;
    @SerializedName("isFirstOrder")
    private boolean isFirstOrder;
    @SerializedName("qualifiesForFreeShipping")
    private boolean qualifiesForFreeShipping;
    @SerializedName("qualifiesForFreeExpressDelivery")
    private boolean qualifiesForFreeExpressDelivery;
    @SerializedName("numUniqueVariants")
    private float numUniqueVariants;
    @SerializedName("hasFreebieItem")
    private boolean hasFreebieItem;
    @SerializedName("numVariants")
    private float numVariants;
    @SerializedName("hasStandardItem")
    private boolean hasStandardItem;
    @SerializedName("hasExpressItem")
    private boolean hasExpressItem;
    @SerializedName("cartVariantsSubtotalValue")
    private Long cartVariantsSubtotalValue;
    @SerializedName("cartEstimatedTotal")
    private Long cartEstimatedTotal;
    @SerializedName("qty")
    private Long qty;
    @SerializedName("name")
    private String name;
    @SerializedName("report")
    private Report ReportObject;

    @SerializedName("variants")
    private ArrayList<Object> variants = new ArrayList<Object>();
    @SerializedName("categoryId")
    private String categoryId;

    @SerializedName("sourceId")
    private String sourceId;
    @SerializedName("newTotalQuantity")
    private float newTotalQuantity;
    @SerializedName("quantity")
    private float quantity;
    @SerializedName("cartId")
    private String cartId;
    @SerializedName("sourceSubArea")
    private float sourceSubArea;
    @SerializedName("sortMethod")
    private String sortMethod;
    @SerializedName("existingUser")
    private boolean existingUser;
    @SerializedName("optIn")
    private boolean optIn;
    @SerializedName("address")
    private Address address;
    @SerializedName("changeType")
    private float changeType;
    @SerializedName("triggeredByUser")
    private boolean triggeredByUser;
    @SerializedName("smartbrand")
    private Smartbrand SmartbrandObject;

    @SerializedName("ipAddress")
    private String ipAddress;
    @SerializedName("referrerUrl")
    private String referrerUrl;
    @SerializedName("performance")
    private Performance PerformanceObject;


    @SerializedName("ip")
    private String ip;
    @SerializedName("accessMethod")
    private Long accessMethod;
    @SerializedName("accessType")
    private Long accessType;
    @SerializedName("text")
    private String text;
    @SerializedName("headerIntent")
    private Long headerIntent;
    @SerializedName("headerPlacement")
    private Long headerPlacement;
    @SerializedName("featureType")
    private Long featureType;
    @SerializedName("moduleName")
    private String moduleName;
    public String getModuleName() {
        return moduleName;
    }
    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public Long getFeatureType() {
        return featureType;
    }

    public void setFeatureType(Long featureType) {
        this.featureType = featureType;
    }


    public String getText() {
        return text;
    }

    public Long getHeaderIntent() {
        return headerIntent;
    }

    public Long getHeaderPlacement() {
        return headerPlacement;
    }

    public String setText() {
        return text;
    }

    public Long setHeaderIntent() {
        return headerIntent;
    }

    public Long setHeaderPlacement() {
        return headerPlacement;
    }

    public Long getAccessMethod() {
        return accessType;
    }

    public void setAccessMethod(Long accessType) {
        this.accessType = accessType;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }


    public String getIpAddress() {
        return ipAddress;
    }

    public String getReferrerUrl() {
        return referrerUrl;
    }

    public Performance getPerformanceObject() {
        return PerformanceObject;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public void setReferrerUrl(String referrerUrl) {
        this.referrerUrl = referrerUrl;
    }

    public void setPerformanceObject(Performance PerformanceObject) {
        this.PerformanceObject = PerformanceObject;
    }

    public Smartbrand getSmartbrand() {
        return SmartbrandObject;
    }

    public void setSmartbrand(Smartbrand SmartbrandObject) {
        this.SmartbrandObject = SmartbrandObject;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public boolean getExistingUser() {
        return existingUser;
    }
    
    public boolean getOptIn() {
        return optIn;
    }

    public void setExistingUser(boolean existingUser) {
        this.existingUser = existingUser;
    }

    public void setOptIn(boolean optIn) {
        this.optIn = optIn;
    }

    public String getSourceId() {
      return sourceId;
    }

    public float getNewTotalQuantity() {
      return newTotalQuantity;
    }
  

    public float getQuantity() {
      return quantity;
    }

    public String getCartId() {
      return cartId;
    }
 
    public float getSourceSubArea() {
      return sourceSubArea;
    }
  
    public String getSourceScreenName() {
      return sourceScreenName;
    }
  
  
    public String getSortMethod() {
      return sortMethod;
    }
  
   
  
    public float getSourceType() {
      return sourceType;
    }
   
    public void setSourceId( String sourceId ) {
      this.sourceId = sourceId;
    }

    public void setNewTotalQuantity( float newTotalQuantity ) {
      this.newTotalQuantity = newTotalQuantity;
    }
  
    public void setQuantity( float quantity ) {
      this.quantity = quantity;
    }

    public void setCartId( String cartId ) {
      this.cartId = cartId;
    }

    public void setSourceSubArea( float sourceSubArea ) {
      this.sourceSubArea = sourceSubArea;
    }
  
    public void setSourceScreenName( String sourceScreenName ) {
      this.sourceScreenName = sourceScreenName;
    }
  
    
 
  


  
    public void setSortMethod( String sortMethod ) {
      this.sortMethod = sortMethod;
    }
  
 
  
    public void setSourceType( Long sourceType ) {
      this.sourceType = sourceType;
    }
  
  













    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public ArrayList<Object> getVariants() {
        return variants;
    }

    public void setVariants(ArrayList<Object> variants) {
        this.variants = variants;
    }

    public Report getReport() {
        return ReportObject;
    }

    public Long getCartVariantsSubtotalValue() {
      return cartVariantsSubtotalValue;
    }
  
    public Long getCartEstimatedTotal() {
      return cartEstimatedTotal;
    }
  
    public Long getQty() {
      return qty;
    }
    
    public String getName() {
      return name;
    }

    public void setReport(Report ReportObject) {
        this.ReportObject = ReportObject;
    }

    public void setCartVariantsSubtotalValue( Long cartVariantsSubtotalValue ) {
      this.cartVariantsSubtotalValue = cartVariantsSubtotalValue;
    }
  
    public void setCartEstimatedTotal( Long cartEstimatedTotal ) {
      this.cartEstimatedTotal = cartEstimatedTotal;
    }

    public void setQty( Long qty ) {
      this.qty = qty;
    }
  
    public void setName( String name ) {
      this.name = name;
    }
  
    public float getCartVariantSubtotalValue() {
        return cartVariantSubtotalValue;
    }

    public boolean getHasSampleItem() {
        return hasSampleItem;
    }

    public float getObgibState() {
        return obgibState;
    }

    public boolean getIsFreeShippingPerkUser() {
        return isFreeShippingPerkUser;
    }

    public String getObgibDeliveryDate() {
        return obgibDeliveryDate;
    }

    public boolean getIsFreeShippingNoLimit() {
        return isFreeShippingNoLimit;
    }


    public boolean getIsFirstOrder() {
        return isFirstOrder;
    }

    public boolean getQualifiesForFreeShipping() {
        return qualifiesForFreeShipping;
    }

    public boolean getQualifiesForFreeExpressDelivery() {
        return qualifiesForFreeExpressDelivery;
    }


    public float getNumUniqueVariants() {
        return numUniqueVariants;
    }

    public boolean getHasFreebieItem() {
        return hasFreebieItem;
    }


    public float getNumVariants() {
        return numVariants;
    }

    public boolean getHasStandardItem() {
        return hasStandardItem;
    }

    public boolean getHasExpressItem() {
        return hasExpressItem;
    }

    public void setCartVariantSubtotalValue( float cartVariantSubtotalValue ) {
        this.cartVariantSubtotalValue = cartVariantSubtotalValue;
    }

    public void setHasSampleItem( boolean hasSampleItem ) {
        this.hasSampleItem = hasSampleItem;
    }

    public void setObgibState( float obgibState ) {
        this.obgibState = obgibState;
    }

    public void setIsFreeShippingPerkUser( boolean isFreeShippingPerkUser ) {
        this.isFreeShippingPerkUser = isFreeShippingPerkUser;
    }

    public void setObgibDeliveryDate( String obgibDeliveryDate ) {
        this.obgibDeliveryDate = obgibDeliveryDate;
    }
    

    public void setIsFreeShippingNoLimit( boolean isFreeShippingNoLimit ) {
    this.isFreeShippingNoLimit = isFreeShippingNoLimit;
    }


    public void setIsLoggedIn( boolean isLoggedIn ) {
    this.isLoggedIn = isLoggedIn;
    }

    public void setIsFirstOrder( boolean isFirstOrder ) {
    this.isFirstOrder = isFirstOrder;
    }

    public void setShouldAutoRenew( boolean shouldAutoRenew ) {
    this.shouldAutoRenew = shouldAutoRenew;
    }



    public void setHasCookiedEmail( boolean hasCookiedEmail ) {
    this.hasCookiedEmail = hasCookiedEmail;
    }



    public void setQualifiesForFreeShipping( boolean qualifiesForFreeShipping ) {
    this.qualifiesForFreeShipping = qualifiesForFreeShipping;
    }

    public void setQualifiesForFreeExpressDelivery( boolean qualifiesForFreeExpressDelivery ) {
    this.qualifiesForFreeExpressDelivery = qualifiesForFreeExpressDelivery;
    }


    public void setNumUniqueVariants( float numUniqueVariants ) {
    this.numUniqueVariants = numUniqueVariants;
    }

    
    public void setHasFreebieItem( boolean hasFreebieItem ) {
    this.hasFreebieItem = hasFreebieItem;
    }


    public void setNumVariants( float numVariants ) {
    this.numVariants = numVariants;
    }


    public void setCurrentCartSharedId( String currentCartSharedId ) {
    this.currentCartSharedId = currentCartSharedId;
    }

    public void setIsReprocessed( boolean isReprocessed ) {
    this.isReprocessed = isReprocessed;
    }

    public void setHasStandardItem( boolean hasStandardItem ) {
    this.hasStandardItem = hasStandardItem;
    }

    public void setHasExpressItem( boolean hasExpressItem ) {
    this.hasExpressItem = hasExpressItem;
    }

    public String getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
    }

    public String getOriginalDeviceId() {
        return originalDeviceId;
    }

    public void setOriginalDeviceId(String originalDeviceId) {
        this.originalDeviceId = originalDeviceId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getPlePosition() {
        return plePosition;
    }

    public Long getPleTotal() {
        return pleTotal;
    }
   
    public Long getPrice() {
        return price;
    }

    public void setPlePosition(Long plePosition) {
        this.plePosition = plePosition;
    }

    public void setPleTotal(Long pleTotal) {
        this.pleTotal = pleTotal;
    }
   
    public void setPrice(Long price) {
        this.price = price;
    }

    public String getEsNumResults() {
        return esNumResults;
    }

    public void setEsNumResults(String esNumResults) {
        this.esNumResults = esNumResults;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }


    public Long getNumResults() {
        return numResults;
    }

    public void setNumResults(Long numResults) {
        this.numResults = numResults;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getEsTime() {
        return esTime;
    }

    public void setEsTime(Long esTime) {
        this.esTime = esTime;
    }


    public String getPleIntent() { 
        return pleIntent;
    }

    public void setPleIntent(String pleIntent) {
        this.pleIntent = pleIntent;
    }

    public Long getPdpIntent() {
        return pdpIntent;
    }

    public void setPdpIntent(Long pdpIntent) {
        this.pdpIntent = pdpIntent;
    }

    public boolean getInStock() {
        return inStock;
    }

    public void setInStock(boolean inStock) {
        this.inStock = inStock;
    }

    public String getVariantId() {
        return variantId;
    }

    public void setVariantId(String variantId) {
        this.variantId = variantId;
    }

    public String getIntentRelatedId() {
        return intentRelatedId;
    }

    public void setIntentRelatedId(String intentRelatedId) {
        this.intentRelatedId = intentRelatedId;
    }

    public Long getOldMembershipStatus() {
        return oldMembershipStatus;
    }

    public void setOldMembershipStatus(Long oldMembershipStatus ) {
        this.oldMembershipStatus = oldMembershipStatus;
    }
    
    public String getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    public String getV() {
        return v;
    }
    
    public void setV(String v) {
        this.v = v;
    }
  
    
    public Long getNewMembershipStatus() {
        return newMembershipStatus;
    }

    public void setNewMembershipStatus(Long newMembershipStatus) {
        this.newMembershipStatus = newMembershipStatus;
    }

    public String getUserId() {
        return userId;
    }
    
    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public ThestralFeatures getThestralFeatures() {
        return thestralFeatures;
    }
    public void setThestralFeatures(ThestralFeatures thestralFeatures) {
        this.thestralFeatures = thestralFeatures;
    }
    public Boolean getHasCookiedEmail() {
        return hasCookiedEmail;
    }
    public void setHasCookiedEmail(Boolean hasCookiedEmail) {
        this.hasCookiedEmail = hasCookiedEmail;
    }
    public String getAppVersion() {
        return appVersion;
    }
    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }
    public String getPostalCode() {
        return postalCode;
    }
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }
    public Object getUtmMedium() {
        return utmMedium;
    }
    public void setUtmMedium(Object utmMedium) {
        this.utmMedium = utmMedium;
    }
    public String getUserAgentBrowser() {
        return userAgentBrowser;
    }
    public void setUserAgentBrowser(String userAgentBrowser) {
        this.userAgentBrowser = userAgentBrowser;
    }
    public Object getUtmExperiment() {
        return utmExperiment;
    }
    public void setUtmExperiment(Object utmExperiment) {
        this.utmExperiment = utmExperiment;
    }
    // public String getVitalTypeDisplay() {
    //     return vitalTypeDisplay;
    // }
    // public void setVitalTypeDisplay(String vitalTypeDisplay) {
    //     this.vitalTypeDisplay = vitalTypeDisplay;
    // }
    public String getManufacturer() {
        return manufacturer;
    }
    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }
    public String getUserAgentOs() {
        return userAgentOs;
    }
    public void setUserAgentOs(String userAgentOs) {
        this.userAgentOs = userAgentOs;
    }
    public QueryParameters getQueryParameters() {
        return queryParameters;
    }
    public void setQueryParameters(QueryParameters queryParameters) {
        this.queryParameters = queryParameters;
    }
    public String getOsVersion() {
        return osVersion;
    }
    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }
    public String getUserAgentString() {
        return userAgentString;
    }
    public void setUserAgentString(String userAgentString) {
        this.userAgentString = userAgentString;
    }
    public Object getUtmPurpose() {
        return utmPurpose;
    }
    public void setUtmPurpose(Object utmPurpose) {
        this.utmPurpose = utmPurpose;
    }
    public Object getUtmAdId() {
        return utmAdId;
    }
    public void setUtmAdId(Object utmAdId) {
        this.utmAdId = utmAdId;
    }
    public String getRemoteAddress() {
        return remoteAddress;
    }
    public void setRemoteAddress(String remoteAddress) {
        this.remoteAddress = remoteAddress;
    }
    public Object getUtmContent() {
        return utmContent;
    }
    public void setUtmContent(Object utmContent) {
        this.utmContent = utmContent;
    }
    // public Double getVitalValue() {
    //     return vitalValue;
    // }
    // public void setVitalValue(Double vitalValue) {
    //     this.vitalValue = vitalValue;
    // }
    public Object getUtmTarget() {
        return utmTarget;
    }
    public void setUtmTarget(Object utmTarget) {
        this.utmTarget = utmTarget;
    }
    public String getOs() {
        return os;
    }
    public void setOs(String os) {
        this.os = os;
    }
    public Object getUtmCampaign() {
        return utmCampaign;
    }
    public void setUtmCampaign(Object utmCampaign) {
        this.utmCampaign = utmCampaign;
    }
    public String getWebSlugConfigId() {
        return webSlugConfigId;
    }
    public void setWebSlugConfigId(String webSlugConfigId) {
        this.webSlugConfigId = webSlugConfigId;
    }
    public String getAppRelease() {
        return appRelease;
    }
    public void setAppRelease(String appRelease) {
        this.appRelease = appRelease;
    }
    public String getHasPurchasedBefore() {
        return hasPurchasedBefore;
    }
    public void setHasPurchasedBefore(String hasPurchasedBefore) {
        this.hasPurchasedBefore = hasPurchasedBefore;
    }
    public String getUserAgent() {
        return userAgent;
    }
    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }
    public String getSessionId() {
        return sessionId;
    }
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
    public Long getMembershipStatus() {
        return membershipStatus;
    }
    public void setMembershipStatus (Long membershipStatus) {
        this.membershipStatus = membershipStatus;
    }
    public String getUserAgentDevice() {
        return userAgentDevice;
    }
    public void setUserAgentDevice(String userAgentDevice) {
        this.userAgentDevice = userAgentDevice;
    }
    public Object getUtmTerm() {
        return utmTerm;
    }
    public void setUtmTerm(Object utmTerm) {
        this.utmTerm = utmTerm;
    }
    public Object getCurrentCartSharedId() {
        return currentCartSharedId;
    }
    public void setCurrentCartSharedId(Object currentCartSharedId) {
        this.currentCartSharedId = currentCartSharedId;
    }
    public Boolean getIsReprocessed() {
        return isReprocessed;
    }
    public void setIsReprocessed(Boolean isReprocessed) {
        this.isReprocessed = isReprocessed;
    }
    public BsProperties getBsProperties() {
        return bsProperties;
    }
    public void setBsProperties(BsProperties bsProperties) {
        this.bsProperties = bsProperties;
    }
    public Boolean getIsLoggedIn() {
        return isLoggedIn;
    }
    public void setIsLoggedIn(Boolean isLoggedIn) {
        this.isLoggedIn = isLoggedIn;
    }
    public Boolean getShouldAutoRenew() {
        return shouldAutoRenew;
    }
    public void setShouldAutoRenew(Boolean shouldAutoRenew) {
        this.shouldAutoRenew = shouldAutoRenew;
    }
    public String getPage() {
        return page;
    }
    public void setPage(String page) {
        this.page = page;
    }
    public String getLibVersion() {
        return libVersion;
    }
    public void setLibVersion(String libVersion) {
        this.libVersion = libVersion;
    }
    public Long getVitalType() {
        return vitalType;
    }
    public void setVitalType(Long vitalType) {
        this.vitalType = vitalType;
    }
    public Object getUtmSource() {
        return utmSource;
    }
    public void setUtmSource(Object utmSource) {
        this.utmSource = utmSource;
    }
}
